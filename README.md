Peers Community
===============================================================================

# About
This is the official repository of the [Peers](http://peers.community)
website.  

# Peers
The *Peers Community* is a community of people with a strong interest in free
software and free culture.  

You can talk to us on [freepost](http://freepo.st/community/Peers), or at
#peers on freenode.  

# License
Copyright 2015 - 2018 vaeringjar.  

Code distributed under the AGPLv3+.  
Media distributed under the CC BY-SA 4.0.  

See the LICENSE.md file for more details...  

# Building

_See the Makefile for more info about building, recipes, and use._  

Title: TODO
Date: 2017-07-19 00:00
Modified: 2018-07-02 00:00
Category: Page
Tags: todo
Slug: todo
Authors: vaeringjar
Description: Welcome to the The Peers Community, a highly motivated community of people with a strong interest in free software and free culture.
Summary: ToDo

<!-- ********************************************************************** -->

# <a id="goals"></a>Goals

In general, The Peers Community has several long term goals:  

<ul>
    <li>
	<strong>Funding.</strong> Peers originally had planned to start as a
	fundraising service for priority and member projects. Not too long
	before Peers started, Snowdrift had already begun
	<a href="https://snowdrift.coop/p/snowdrift/w/en/othercrowdfunding">research</a>
	on a free/libre crowdfunding service.
    </li>
    <li>
	<strong>Prioritization.</strong> We plan to follow the example of
	the FSF which has their own
	<a href="https://www.fsf.org/campaigns/priority-projects">list of high priority projects</a>.
	Peers has a list of <a href="https://notabug.org/peers/peers-bounty-hunts" title="Project Ideas">project ideas</a>,
	though at this time cannot offer bounties in general
    </li>
    <li>
	<strong>Reducing Compromise.</strong> Not everyone leaves proprietary software in a single bound, in fact it might take a long time to achieve. Watch or listen to a roundtable discussion on the need (or lack of need) for <a href="https://archive.org/details/CompromiseInFreeSoftware">compromise in Free Software</a>. Hosted by Bryan Lunduke with panelists including: Richard M Stallman, Aaron Seigo, Swapnil Bhartiya, and Stuart Langridge.
    </li>
</ul>


---
# <a id="kanban"></a>Kanban

<a href="https://oasis.sandstorm.io/shared/qfDmo-ZMNMr972-Q2_2AkU7FQ9SpaBDXXaQIbUwF090" target="_blank">The Peers Kanban</a>
uses an instance of <a href="https://wekan.io/">Wekan</a> for a high level
list of tasks for Peers. For now have have hosted it on
<a href="https://oasis.sandstorm.io/">Oasis</a> until we can transfer it to
one of our local servers.


---
# <a id="projects"></a>Projects

For specific tasks, please visit the issue trackers for each respective
[Peers project](../#projects).  


<footer>
    <small>&copy; 2018 Peers</small>
    <small>&bull; Website Code distributed under the AGPLv3+</small>
    <small>&bull; Website Media licensed under CC BY-SA 4.0</small>
</footer>

Title: Why?
Date: 2017-07-19 00:00
Modified: 2018-07-02 00:00
Category: Page
Tags: why
Slug: why
Authors: vaeringjar
Description: Welcome to the The Peers Community, a highly motivated community of people with a strong interest in free software and free culture.
Summary: Why?

<!-- ********************************************************************** -->

# <a id="reasons"></a>Reasons

We have many reasons, in general, why to use free software and to use
decentralized, private/anonymous services, instead of proprietary software or
services.  

**Ethical reason:** we all benefit by sharing knowledge  

**Personal reason:** with free software, our computers do what we want them to 
do! On the other hand, with proprietary software computers are controlled by 
other people.  

**Economic reason:** users lock-in. Switching away from proprietary software can
require significant time or financial costs.  

**Convenience:** forced to do what a company thinks best (for example you must
use their software or files type)  

**Privacy:** you become constantly watched by somebody who can do pretty much
whatever they want with your personal data, because you've "agreed" with the
"license agreement" that they can. This is by very far the most serious argument
that people seem not to understand, because they always say "so what? What I'm
doing with their service is of little value anyway!".  

---

# <a id="testamonials"></a>Testamonials and Examples

We searched the web for Non-Peers Community people who had shared their own experiences and expertise.  

* Peter Swire testifies before the Senate Judiciary Committee on [encryption and the balance between public safety and privacy](https://fpf.org/wp-content/uploads/2015/07/SwireCrypto070715.pdf).
* Drew DeVault explains why we should [use IRC instead of slack](https://drewdevault.com/2015/11/01/Please-stop-using-slack.html).
* Alberto of flosspirit went [one year without Whatsapp](https://flosspirit.wordpress.com/2015/03/07/1-ano-sin-whatsapp/) (en español).
* Whatsapp blocks users from Telegram from [communicating with each other](https://orat.io/blog/as-of-today-whatsapp-is-blocking-telegram-links/).
* The FSF promotes free software awareness in their video [User Liberation](https://www.fsf.org/blogs/community/user-liberation-watch-and-share-our-new-video).
* Robin Doherty explains [Why privacy is important, and having &quot;nothing to hide&quot; is irrelevant](http://robindoherty.com/2016/01/06/nothing-to-hide.html).
* Erik Möller discusses licensing in [The case for Free use: reasons not to use a Creative Commons - NC license](http://freedomdefined.org/Licenses/NC).
* Dr. Tarek Loubani speaks out how [free software and hardware become essential to sovereignty among developing nations](https://media.ccc.de/v/32c3-7246-free_software_and_hardware_bring_national_sovereignty), and how we can use them to secure infrastructure and information against sophisticated adversaries.
* Actor, comedian, and technologist Stephen Fry summaries the [early history of GNU and Linux and the pragmatic reasons for using free software](https://gnu.org/fry/).
* Minifree maintains their own [list of how and why proprietary payment services restrict user freedoms](https://minifree.org/paypal/).
* PRISM Break maintains a [website to protect users from insecure internet usage](https://prism-break.org/), specifically related to privacy, citing only free software and with all website [licensed under the GPL](https://github.com/nylira/prism-break/blob/master/LICENSE.md).

---

# <a id="recommendations"></a>Recommendations

As an alternative to some proprietary communications, and gaming platforms, we
recommend the following. For a catalogue of free software in general, see
[The Free Software Directory](https://directory.fsf.org/wiki/Main_Page).

The FSF also has their own list of recommendations for
[secure and private computing](https://directory.fsf.org/wiki/Collection:PRISM).

For choosing a free system, GNU recommends a selection of [free distros](https://www.gnu.org/distros/free-distros.html).

Also, note that [GNU](https://www.gnu.org/links/companies.html) and
[Replicant](http://blog.replicant.us/2015/12/shops-selling-devices-pre-installed-with-replicant/)
both have hardware supplier recommendations.  

---

<table class="table">
    <tbody>
        <tr>
            <td><a href="https://www.arduino.cc/">Arduino/Genuino</a></td>
            <td></td>
            <td>Libre prototyping platform based on easy-to-use hardware and software (<a href="https://www.arduino.cc/en/Main/FAQ">FAQ</a>)</td>
        </tr>


        <tr>
            <td><a href="https://f-droid.org/">F-Droid</a></td>
            <td><a href="https://gitlab.com/fdroid/fdroidclient">source</a></td>
            <td>Android application package manager</td>
        </tr>


        <tr>
            <td><a href="https://www.mozilla.org/firefox/">Firefox</a></td>
            <td><a href="https://developer.mozilla.org/en-US/docs/Mozilla/Developer_guide/Source_Code/Downloading_Source_Archives">source</a></td>
            <td>Free web browser by Mozilla</td>
        </tr>


        <tr>
            <td><a href="https://freedomboxfoundation.org/">FreedomBox</a></td>
            <td></td>
            <td>A personal server that protects privacy. Also, the project maintains a <a href="https://wiki.debian.org/FreedomBox/Hardware">list of free hardware</a> over at the Debian wiki.</td>
        </tr>


        <tr>
            <td><a href="http://www.gimp.org/">GIMP</a></td>
            <td><a href="http://www.gimp.org/downloads/">source</a></td>
            <td>cross-platform image editor</td>
        </tr>


        <tr>
            <td><a href="http://gnu.io/social/">gnusocial</a></td>
            <td><a href="http://gnu.io/social/resources/code/">source</a></td>
            <td>Federated social networking/communication software for both public and private communications.</td>
        </tr>


        <tr>
            <td><a href="https://www.gnu.org/software/gnuzilla/">IceCat</a></td>
            <td><a href="http://git.savannah.gnu.org/cgit/gnuzilla.git">source</a></td>
            <td>GNU version of the Firefox browser. Also, with the FSF directory list of <a href="https://directory.fsf.org/wiki/IceCat">free addons</a>.</td>
        </tr>


        <tr>
            <td><a href="http://keepass.info/">keepass2</a></td>
            <td><a href="http://sourceforge.net/p/keepass/code/">svn</a></td><!--<a href="https://github.com/cappert/keepass2">unofficial git mirror</a>-->
            <td>A light-weight and easy-to-use password manager.</td>
        </tr>


        <tr>
            <td><a href="http://www.kontalk.org/">Kontalk</a></td>
            <td><a href="https://github.com/kontalk">source</a></td>
            <td>End to end encrypted chat, based on XMPP</td>
        </tr>


        <tr>
            <td><a href="https://libregamewiki.org/">Libregamewiki</a></td>
            <td></td>
            <td>A wiki of free games and related topics by Han Dao. All documentation under dual GNU FDLv1.2 and CC-BY 3.0. Powered by <a href="https://www.mediawiki.org/">MediaWiki</a></td>
        </tr>


        <tr>
            <td><a href="https://www.libreoffice.org/">LibreOffice</a></td>
            <td><a href="https://www.libreoffice.org/download/">source</a></td>
            <td>A powerful office suite, including word processor, spreadsheets, and slidedeck maker.</td>
        </tr>


        <tr>
            <td><a href="http://www.minetest.net/">Minetest</a></td>
            <td><a href="https://github.com/minetest/minetest">source</a></td>
            <td>An infinite-world block sandbox game with survival and crafting.</td>
        </tr>


        <tr>
            <td><a href="http://www.mumble.info/">Mumble</a></td>
            <td><a href="https://github.com/mumble-voip/mumble">source</a></td>
            <td>A voicechat (VoIP) program originally implemented for gamers written on top of Qt and Speex.</td>
        </tr>


        <tr>
            <td><a href="http://www.openphoenux.org/">OpenPhoenux</a></td>
            <td></td>
            <td>Independent Mobile Tool Community, whose devices run free software.</td>
        </tr>


        <tr>
            <td><a href="http://www.replicant.us/">Replicant</a></td>
            <td><a href="https://git.replicant.us/">source</a></td>
            <td>A free fork of Android.</td>
        </tr>


        <tr>
            <td><a href="https://www.videolan.org/vlc/">VLC Media Player</a></td>
            <td><a href="https://www.videolan.org/vlc/download-sources.html">source</a></td>
            <td>A cross-platform multimedia player and framework that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and various streaming protocols.</td>
        </tr>

    </tbody>
</table>

<footer>
    <small>&copy; 2018 Peers</small>
    <small>&bull; Website Code distributed under the AGPLv3+</small>
    <small>&bull; Website Media licensed under CC BY-SA 4.0</small>
</footer>

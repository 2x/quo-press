Title: Home
Date: 2017-07-19 00:00
Modified: 2018-08-06 00:00
Category: Page
Tags: home
Slug: home
Authors: vaeringjar
Description: Welcome to the The Peers Community, a highly motivated community of people with a strong interest in free software and free culture.
Summary: home
URL:
save_as: index.html

<!-- ********************************************************************** -->

<header class="home">
    <div style="">
        <h1 style="padding-top: 400px; font-size: xx-large;">
            The Peers Community
        </h1>
        <p>
            For
            <a href="https://en.wikipedia.org/wiki/Free_software">free software</a>
            and
            <a href="https://en.wikipedia.org/wiki/Free_culture_movement">free culture</a>.
        </p>
        <a href="#contact">Join Peers</a>
        <small>Or just say hello.</small>
    </div>
</header>

<section title="About">
    <h2><a id="about" style="display:none;"></a>About</h2>    
    <p>
        Welcome to <strong>The Peers Community</strong>, a highly motivated
        community of people with a strong interest in
        <a href="https://en.wikipedia.org/wiki/Free_software">free software</a>
        and
        <a href="https://en.wikipedia.org/wiki/Free_culture_movement">free culture</a>.
    </p>
    <p>
	We all help each other toward the common goal of supporting and growing
	free projects. The Peers Community provides a list of independent
	projects that all strive for free (as in freedom!) software and culture.
	If you would like to learn more about a particular project, feel free to
	reach out to them directly.
    </p>
</section>

<section title="Projects">
    <h2><a id="projects" style="display:block;"></a>Projects</h2>
    <p><strong>The following is a list of projects by people participating in our community.</strong></p>
    <p>If you would like to join us and add your project, feel free to contact us.</p>

    <table class="table">
        <tbody>
            <tr>
                <td>
		    <a href="https://addons.mozilla.org/en-US/firefox/addon/archive-webextension/">archive-webextension</a>
		    (<a href="https://notabug.org/zPlus/archive-webextension">source</a>)
		</td>
                <td><span>zPlus</span></td>
                <td>WebExtension to save web pages to the Internet Archive Wayback Machine.</td>
            </tr>

            <tr>
                <td><a href="https://notabug.org/Calinou/awesome-gamedev">awesome-gamedev</a></td>
                <td><span>Calinou</span></td>
                <td>A collection of free software and free culture resources for making amazing games.</td>
            </tr>

            <tr>
                <td>
		    <a href="https://gnome-look.org/content/show.php?content=166224">Digit's Big Tiny Font Collection</a>
		    and
		    <a href="https://box-look.org/content/show.php?content=174580">Collection 2</a>
		    (<a href="https://notabug.org/digit/dbtfc">sources</a>)
		</td>
                <td><span>Digit</span></td>
                <td>A big collection of (mostly) tiny bitmap fonts.</td>
            </tr>

            <tr>
                <td><a href="https://notabug.org/bkeys/DMUX">DMUX</a></td>
                <td><span>bkeys</span></td>
                <td>A game about customizing cars, joining a team and fighting other cars, however not all game modes are this way.</td>
            </tr>

            <tr>
                <td><a href="https://dokk.org/about">DOKK</a>
				(<a href="https://notabug.org/DOKK/dokk">source</a>)</td>
                <td><span>zPlus</span></td>
                <td>A project aimed at combining information from free knowledge
                    graphs and organizing it into meaningful topics.</td>
            </tr>

            <tr>
                <td><a href="https://dragora.org">Dragora</a></td>
                <td><span>selk, mechaniputer</span></td>
                <td>
                    The Dragora project produces a libre, reliable, GNU/Linux distribution made from scratch.
                    Dragora respects the freedom of the user with the values of free software and provides control to those who use it.
                    Dragora is developed entirely by volunteers and it is published under the terms of the GNU General Public License.
                </td>
            </tr>

            <tr>
                <td><a href="https://notabug.org/pizzaiolo/freedom-delayed">freedom-delayed</a></td>
                <td><span>pizzaiolo</span></td>
                <td>Checklist for projects that have promised to liberate software in the future (but so far, haven't)</td>
            </tr>

            <tr>
                <td><a href="https://freeyourstuff.cc">Free your stuff!</a> (<a href="https://notabug.org/freeyourstuff/freeyourstuff.cc">source</a>)</td>
                <td><span>Eloquence</span></td>
                <td>A content liberation extension for Chromium, for supported websites.</td>
            </tr>

            <tr>
                <td><a href="https://freepo.st">freepost</a> (<a href="https://notabug.org/zPlus/freepost">source</a>)</td>
                <td><span>zPlus, vaeringjar</span></td>
                <td>A free discussion board</td>
            </tr>

            <tr>
                <td><a href="https://notabug.org/fr33domlover/funbot">funbot</a> and <a href="https://notabug.org/fr33domlover/fpbot">fpbot</a></td>
                <td><span>fr33domlover</span></td>
                <td>An IRC bot for learning, fun and collaboration in the Freepost community.</td>
            </tr>

            <tr>
                <td><a href="https://gnusocial.no/">gnusocial.no</a>
				(<a href="https://git.gnu.io/gnu/gnu-social">source</a>)</td>
                <td><span>vaeringjar</span></td>
                <td>Instance of the gnusocial federated social network.</td>
            </tr>

            <tr>
                <td>
		    <a href="https://f-droid.org/repository/browse/?fdfilter=radio&fdid=community.peers.internetradio">InternetRadio</a>
		    (<a href="https://notabug.org/metadb-apps/InternetRadio">sources</a>)
		</td>
                <td><span>Nathan, zPlus</span></td>
                <td>Simple Android app for searching and listening to Internet radio stations.</td>
            </tr>

            <tr>
                <td><a href="https://libreboot.org">libreboot</a> (<a href="https://libreboot.org/git/">sources</a>)</td>
                <td><span>leah</span></td>
                <td>
		    Libreboot is a free (libre) BIOS/UEFI replacement.
		    Based on coreboot, the aim is to distribute low-level boot firmware that is 100% free software, and easy to use.
		</td>
            </tr>

            <tr>
                <td><a href="https://lib.reviews">lib.reviews</a> (<a href="https://notabug.org/freeyourstuff/lib.reviews">source</a>)</td>
                <td><span>Eloquence</span></td>
                <td>A free, open and not-for-profit platform for reviewing absolutely anything, in any language.</td>
            </tr>

            <tr>
                <td><a href="https://f-droid.org/repository/browse/?fdid=community.peers.license">License</a> (<a href="https://notabug.org/metadb-apps/license">sources</a>)</td>
                <td><span>Nathan, zPlus</span></td>
                <td>App for Android to search software licenses and read their full text version, as defined by SPDX.</td>
            </tr>

            <tr>
                <td><a href="https://meta.peers.community"><em>META</em></a></td>
                <td><span>tuna</span></td>
                <td>Collection of metadata</td>
            </tr>

            <tr>
                <td><a href="https://metadb.peers.community/">MetaDB</a></td>
                <td><span>zPlus</span></td>
                <td>A public database consisting only of free/libre data, aimed to help other projects and people in the free software community to access common information.</td>
            </tr>

            <tr>
                <td><a href="https://notabug.org">NotABug</a> (<a href="https://notabug.org/hp/gogs">source</a>)</td>
                <td><span>TMM</span></td>
                <td>Website for collaborating on free projects and free documentation using Git</td>
            </tr>

            <tr>
                <td>
		    <a href="#there_you_are" id="hacks_where_ever_you_go">peers</a>
		    (<a href="https://notabug.org/peers/peers-www">source</a>)
		</td>
                <td><span>vaeringjar, zPlus</span></td>
                <td>
                    Code for <a href="https://peers.community">Peers Community</a> website and
                    <a href="https://notabug.org/peers">organization</a> repositories.
                </td>
            </tr>

            <tr>
                <td><a href="https://notabug.org/kl3/spm">spm</a></td>
                <td><span>kl3</span></td>
                <td>Simple Password Manager</td>
            </tr>

            <tr>
                <td><a href="https://dev.angeley.es/s/fr33domlover/r/vervis">Vervis</a>
				(<a href="https://dev.angeley.es/">demo instance</a>)</td>
                <td><span>fr33domlover</span></td>
                <td>Vervis is a project hosting and management application,
                    with a focus on software projects and decentralization.
                    It's still in very early development.</td>
            </tr>
        </tbody>
    </table>
</section>

<section title="Companies">
    <h2><a id="companies" style="display:block;"></a>Companies</h2>
    <table class="table">
        <tbody>
            <tr>
                <td><a href="https://minifree.org/">minifree</a></td>
                <td><span>leah</span></td>
                <td>Minifree provides computer systems with the fully free Libreboot BIOS replacement and Trisquel GNU+Linux, preinstalled, with FSF Respects Your Freedom certification</td>
            </tr>

            <tr>
                <td><a href="https://vikings.net">Vikings</a></td>
                <td><span>Vikings GmbH</span></td>
                <td>
                    Vikings is the world's first professional hosting provider with a fully libre hosting stack, fully respecting the freedom and privacy of users.
		    All services are based entirely on libre boot firmware, libre software and are powered by certified green energy.
                    <br />
                    Vikings is a (partly) crowdfunded project, and <a href="https://store.vikings.net/crowdfunding-infos">it is still raising money</a>.
		    Please consider supporting the project with a donation.
                    <br />
                    Vikings also offers a <a href="https://store.vikings.net">store</a> to buy
		    <a href="https://www.fsf.org/resources/hw/endorsement/respects-your-freedom">Respects-Your-Freedom certified</a> hardware.
                </td>
            </tr>
        </tbody>
    </table>
</section>

<section>
    <h2><a id="inactive" style="display:block;"></a>Inactive Projects</h2>
    <p>These projects are no longer actively developed or maintained, but not necessarily dead.</p>

    <table class="table">
        <tbody>
            <tr>
            <td><a href="https://mirrors.peers.community/mirrors/librepup/website/">librepup</a></td>
                <td><span>dimkr</span></td>
                <td>GNU/Linux-libre distribution, a libre fork of Puppy Linux that uses Trisquel packages.</td>
            </tr>

            <tr>
                <td><a href="https://paste.rel4tion.org/">toothpaste</a> (<a href="https://notabug.org/fr33domlover/toothpaste">sources</a>)</td>
                <td><span>fr33domlover</span></td>
                <td>Paste server written in Haskell. Fork of Hpaste, fully freedom and privacy respecting and generally improved.</td>
            </tr>
        </tbody>
    </table>
</section>

<section>
    <h2><a id="jam" style="display:block;"></a>Projects Jam</h2>
    <p>
        Every Friday around 19:00Z - 21:00Z, we meet on irc.freenode.net #peers
        to collaborate on active Peers projects. Everybody has the opportunity to
        ask for help with any open issues in their projects, and we work together
        to try to fix them. We have a short list of quick <a href="https://notabug.org/peers/project-jams">project jam ideas</a>.
    </p>
    <p>
        Would you like to help too? Feel free to join us! It's a good opportunity
        to help a free project, and also to get to know us! You can also read about
        past Jams in <a href="/category/blog.html">the blog section</a>.
    </p>
</section>

<section>
    <h2><a id="contact" style="display:block;"></a>Contact</h2>
    <p>Reach us via one of the following methods:</p>
    <ul class="contacts">
        <li>
	    <strong>Leave a note</strong> over on <a href="https://freepo.st">freepost</a>
	    or open an issue on the <a href="https://notabug.org/peers/peers-forum">peers-forum</a> issue tracker.
	</li>
        <li><strong>Email us</strong> by sending to: <code>contact <em>at</em> peers <em>dot</em> community</code></li>
        <li><strong>Chat with us</strong> on <code>irc.freenode.net</code> in our channel <code>#peers</code> or on <code>Matrix</code> at <code>#peers:matrix.org</code></li>
    </ul>
</section>

<section>
    <h2><a id="acknowledgements" style="display:block;"></a>Acknowledgements</h2>
    <p>Our logo comes from <a href="https://en.wikipedia.org/wiki/Glider_%28Conway%27s_Life%29">The Glider</a>, the logo of which belongs in the public domain.</p>
    <p>Page banner graphic by <a href="http://www.whitepaperfox.com">White Paper Fox</a>.</p>
    <p>Also, for an honourable mention, we <a href="https://mirrors.peers.community/">mirror</a> some of our favourite projects.</p>
</section>

<section>
    <h2><a id="disclaimer" style="display:block;"></a>Disclaimer</h2>
    <p>The Peers Community only provides information about other projects.</p>
</section>

<footer>
    <small>&copy; 2018 Peers</small>
    <small>&bull; Website Code distributed under the <abbr title="GNU Affero General Public License, version 3 or greater">AGPLv3+</abbr></small>
    <small>&bull; Website Media licensed under <abbr title="Creative Commons Attribution-ShareAlike 4.0 International Public License">CC BY-SA 4.0</abbr></small>
</footer>

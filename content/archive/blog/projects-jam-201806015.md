Title: Projects Jam
Date: 2018-06-15 00:00
Modified: 2018-06-15 00:00
Category: blog
Tags: projects
Slug: projects-jam-20180615
Authors: zPlus
Description: Join us weekly on irc.freenode.net #peers on Thursday and/or Friday target start time around 19:00Z - 21:00Z to review of active Peers projects.
Summary: June 15th session of project jamming.

<!-- ********************************************************************** -->

### Highlights

Fixed https://notabug.org/metadb-apps/InternetRadio/issues/30  
Fixed with Nathan https://notabug.org/metadb-apps/InternetRadio/issues/25  

Nathan also worked on https://notabug.org/peers/peers-www/issues/1 and submitted
PR https://notabug.org/peers/peers-www/pulls/12  

vaeringjar worked on https://notabug.org/peers/peers-template/issues/2 and
also started working on https://notabug.org/peers/peers-www/issues/11  

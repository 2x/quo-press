Title: Projects Jam
Date: 2018-03-16 00:00
Modified: 2018-03-19 19:30
Category: blog
Tags: projects
Slug: projects-jam-20180316
Authors: zPlus
Description: Join us weekly on irc.freenode.net #peers on Thursday and/or Friday target start time around 19:00Z - 21:00Z to review of active Peers projects.
Summary: Third session of project jamming.

### Highlights

- I looked into the InternetRadio SSL/TLS issue (I don't plan to fix it because
it seems to be only an error with older APIs)
- I looked more into VLC or any other player. I think VLC could fix all the
problems with streams that don't play right now, but compiling it is a pain.
Somebody in #vlc said that they will publish a pre-compiled library sooner or
later
- TMM updated Gogs (upstream merge) that fixed a couple of issues https://notabug.org/hp/gogs/issues/158 https://notabug.org/hp/gogs/issues/95 https://notabug.org/hp/gogs/issues/44 https://notabug.org/hp/gogs/issues/43
Title: Projects Jam
Date: 2018-03-30 00:00
Modified: 2018-03-30 20:20
Category: blog
Tags: projects
Slug: projects-jam-20180330
Authors: vaeringjar
Description: Join us weekly on irc.freenode.net #peers on Thursday and/or Friday target start time around 19:00Z - 21:00Z to review of active Peers projects.
Summary: March 30th session of project jamming.

### Discussion

zPlus and I (vaeringjar) exchanged a few thoughts on whether or not to work on a
project that would package free games. I thought about it a few months ago after
reading something on freepo.st and again today after reading about Flare and
[itch client](https://itch.io/app), perhaps a fork of itch for libre-only games.
However, it seems like focusing on an existing package system would do the same.
Or putting effort into making packaging easier for an existing system,
especially, in my opinion, guix. So pin this for later...  

### Highlights

- zPlus migrated to using libvlc in InternetRadio and vaeringjar helped as an
extra set of eyes.
- nathan added copy-to-clipboard functionality to InternetRadio.
- fr33domlover continued to work on Vervis and it is now officially an active Peers project again!
- vaeringjar worked on another project idea for peers-bounty-hunts to create a
web identity provider based on OpenID to allow SSI sign-on/auth.

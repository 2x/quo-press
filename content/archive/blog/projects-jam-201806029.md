Title: Projects Jam
Date: 2018-06-29 00:00
Modified: 2018-07-02 00:00
Category: blog
Tags: projects
Slug: projects-jam-20180629
Authors: zPlus
Description: Join us weekly on irc.freenode.net #peers on Thursday and/or Friday target start time around 19:00Z - 21:00Z to review of active Peers projects.
Summary: June 29th session of project jamming.

<!-- ********************************************************************** -->

### Highlights

Work done to fix:  

- https://notabug.org/metadb-apps/InternetRadio/issues/33
- https://notabug.org/peers/peers-template/issues/3
- https://notabug.org/peers/peers-www/pulls/13

Additionally, we started a discussion about changing the hosting provider,
possibly by moving all the projects hosted at DreamHost to Vikings.  

Title: Projects Jam
Date: 2018-07-27 00:00
Modified: 2018-08-06 00:00
Category: blog
Tags: projects
Slug: projects-jam-20180727
Authors: zPlus
Description: Join us weekly on irc.freenode.net #peers on Thursday and/or Friday target start time around 19:00Z - 21:00Z to review of active Peers projects.
Summary: July 27th session of project jamming.

<!-- ********************************************************************** -->

### Highlights

Today it was only vaeringjar and zPlus.  
We worked on InternetRadio to fix issue
[InternetRadio/issues/34](https://notabug.org/metadb-apps/InternetRadio/issues/34)
and also add Icelandic translation.  

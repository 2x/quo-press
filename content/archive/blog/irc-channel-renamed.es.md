Title: Canal IRC Renombrado
Date: 2017-09-04 02:43
Category: blog
Tags: irc
Slug: irc-channel-renamed
Authors: zPlus
Summary: #freepost se mueve a #peers
Lang: es
url: es/irc-channel-renamed/
save_as: es/irc-channel-renamed/index.html

Se sugirió que `#peers` sería un nombre más apropiado para
la comunidad, así que estamos mudándonos al nuevo canal. `#freepost`
está abierto por ahora, pero tarde o temprano lo redireccionaré a
`#peers`.

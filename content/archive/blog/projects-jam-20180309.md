Title: Projects Jam
Date: 2018-03-09 00:00
Modified: 2018-03-19 14:22
Category: blog
Tags: projects
Slug: projects-jam-20180309
Authors: vaeringjar
Description: Join us weekly on irc.freenode.net #peers on Thursday and/or Friday target start time around 19:00Z - 21:00Z to review of active Peers projects.
Summary: Second session of project jamming.

### Overview

Today I could not participate as much as I would have liked. zPlus took most of the lead today.

### Highlights

* zPlus worked on the InternetArchive plugin. It now has a context menu option when clicking a link that will ask if it should backup the link, rather than the current page. I find this very helpful because now I can browser more pages in HTTPS only mode and without running JavaScript because the wayback machine will handle the rest.
* Calinou mentioned less interest in maintaining awesome-gamedev. Perhaps we can fork or handoff the project later if others would like to maintain it.
* Coincidentally, TMM migrated notabug to better hardware. The outage makes me wish for decentralized git issue tracking and [repo mirrors](https://notabug.org/vaeringjar/peers-mirrors/issues/18).
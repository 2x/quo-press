Title: Sesión de Proyectos
Date: 2018-03-04 03:24
Category: blog
Tags: projectos
Slug: projects-jam-20180302
Authors: vaeringjar
Description: Únete a nosotras semanalmente en irc.freenode.net #peers el jueves y/o viernes tiempo deseado de inicio alrededor de 19:00Z - 21:00Z para la revisión de proyectos Peers arctivos.
Summary: Primera sesión completa de proyectos y un resumen del día anterior.
Lang: es

### Prólogo

Ayer, después de echar un vistazo a el uso de spm, decidí que debería
tomar el siguiente paso en involucrarme más en proyectos. [Publiqué](https://freepo.st/post/8rqbdi1yup) mi
idea:

>[Esto es una traducción del texto original] Quiero comenzar algo
>semanal de «echemos un vistazo a la(s) incidencia(s) de proyectos
>Peers». Mientras que haga esto sola, creo que lo haré o en jueves o
>viernes, probablemente cerca de mi cena que suele ocurrir de 19:00Z a
>21:00Z. Pero si otras quieren planificar un tiempo de reunión en
>#peers, hacédmelo saber y podemos programarlo.

Entonces [respondí una única pregunta](https://notabug.org/kl3/spm/issues/4) en el gestor de incidencias de spm.

### Comienzo

Hoy decidí echar un vistazo a freedom-delayed, probablemente uno de mis
proyectos favoritos que no es código. En general, no cambió mucho.
Principalmente usé archive.org para verificar viejas referencias y para
guardar la versión actual. También usé Wikipedia para ver si alguien
había hecho referencia o discutido cualquiera de los proyectos.
fr33domlover y bkeys se unierón en el chat, trabajando en sus
propios respectivos proyectos.

### Aspectos destacados menores

Tuve problemas cargando la referencia a la promesa de *Dwarf Fortress* de
liberar el código, así que [abrí una
incidencia](https://notabug.org/pizzaiolo/freedom-delayed/issues/15) en
el gestor. Pocket puede se que haya, o casi, liberado. Como no usuaria,
estoy insegura. https://github.com/Pocket/pocket-ff-addon
Tampoco pude cargar zynaddsubfx.

Title: Projects Jam
Date: 2018-07-06 00:00
Modified: 2018-07-22 00:00
Category: blog
Tags: projects
Slug: projects-jam-20180706
Authors: zPlus
Description: Join us weekly on irc.freenode.net #peers on Thursday and/or Friday target start time around 19:00Z - 21:00Z to review of active Peers projects.
Summary: July 6th session of project jamming.

<!-- ********************************************************************** -->

### Highlights

Today we began working on the RDF vocabulary for the repository-federation
project. A new repository was created at https://notabug.org/peers/repo-fed-voca
b to collaborate on the writing of the vocabulary.  

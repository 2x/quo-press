Title: Projects Jam
Date: 2018-06-08 00:00
Modified: 2018-06-10 00:00
Category: blog
Tags: projects
Slug: projects-jam-20180608
Authors: vaeringjar
Description: Join us weekly on irc.freenode.net #peers on Thursday and/or Friday target start time around 19:00Z - 21:00Z to review of active Peers projects.
Summary: June 8th session of project jamming.

<!-- ---------------------------------------------------------------------- -->

### Highlights

* I attempted to build dragora 3 on freeBSD 12-CURRENT. Please note, I have hardware that supports dragora, but apparently not libertyBSD. That said, 12-CURRENT seems to have some significant issues. Will return to this after reinstalling a different image, bare metal on the target hardware.
* Nathan worked on InternetRadio.
* bill-auger, out of band worked on setting up a list for [gitpub](https://framalistes.org/sympa/arc/git-federation/).

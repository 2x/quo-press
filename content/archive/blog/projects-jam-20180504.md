Title: Projects Jam
Date: 2018-05-04 00:00
Modified: 2018-05-15 23:43
Category: blog
Tags: projects
Slug: projects-jam-20180504
Authors: vaeringjar
Description: Join us weekly on irc.freenode.net #peers on Thursday and/or Friday target start time around 19:00Z - 21:00Z to review of active Peers projects.
Summary: May 4th session of project jamming.

### Highlights

Started investigating how to either port or clone the pleroma-fe for use with a gnusocial instance sans qvitter.  
https://notabug.org/peers/peers-bounty-hunts/src/master/projects/preliminary-ideas.md#fediverse-fe  

zPlus worked on:  

Archive-Webextension:  

- fix https://notabug.org/zPlus/archive-webextension/issues/5
- close https://notabug.org/zPlus/archive-webextension/issues/2

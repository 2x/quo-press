Title: IRC Channel Renamed
Date: 2017-08-30 00:00
Category: blog
Tags: irc
Slug: irc-channel-renamed
Authors: zPlus
Description: Welcome to the The Peers Community, a highly motivated community of people with a strong interest in free software and free culture.
Summary: #freepost moving to #peers

It was suggested that <code>#peers</code> would be a more appropriate name for the community, so we're moving to the new channel.  
<code>#freepost</code> is still open for now, but sooner or later I will forward it to <code>#peers</code>.  


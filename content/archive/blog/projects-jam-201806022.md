Title: Projects Jam
Date: 2018-06-22 00:00
Modified: 2018-07-02 00:00
Category: blog
Tags: projects
Slug: projects-jam-20180622
Authors: zPlus
Description: Join us weekly on irc.freenode.net #peers on Thursday and/or Friday target start time around 19:00Z - 21:00Z to review of active Peers projects.
Summary: June 22nd session of project jamming.

<!-- ********************************************************************** -->

### Highlights

On this weekly meetup we discussed about ActivityPub and we talked about the
progress of Vervis. I also used this time for looking at PDF->HTML converters
for the DOKK library, with the help of vaeringjar.  

Title: Projects Jam
Date: 2018-04-27 00:00
Modified: 2018-05-04 19:03
Category: blog
Tags: projects
Slug: projects-jam-20180427
Authors: vaeringjar
Description: Join us weekly on irc.freenode.net #peers on Thursday and/or Friday target start time around 19:00Z - 21:00Z to review of active Peers projects.
Summary: April 27th session of project jamming.

### Highlights

I continued working on a design for an SSI interface that leverages an OpenID
Connect infrastructure.  

zPlus worked on quite a few tasks:  

archive-webextension  

- Fixed https://notabug.org/zPlus/archive-webextension/issues/3
- and https://notabug.org/zPlus/archive-webextension/issues/4
- Released v3.0

InternetRadio  

- Fixed https://notabug.org/metadb-apps/InternetRadio/issues/32

peers-www  

- Pull Request https://notabug.org/peers/peers-www/pulls/7

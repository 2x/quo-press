Title: Projects Jam
Date: 2018-08-03 00:00
Modified: 2018-08-06 00:00
Category: blog
Tags: projects
Slug: projects-jam-20180803
Authors: zPlus
Description: Join us weekly on irc.freenode.net #peers on Thursday and/or Friday target start time around 19:00Z - 21:00Z to review of active Peers projects.
Summary: August 3rd session of project jamming.

<!-- ********************************************************************** -->

### Highlights

Fixed:  

* https://notabug.org/metadb-apps/InternetRadio/pulls/37
* https://notabug.org/zPlus/freepost/issues/75

Closed:  

* https://notabug.org/peers/peers-forum/issues/1 because of lack of activity

Cleaned up the list at https://notabug.org/peers/project-jams (remove solved
issues that are still on the list).  

I also went over the freedom-delayed list, but nothing has changed except maybe
for Pocket (partially).  

And vaeringjar investigated [an issue on gnusocial.no](https://notabug.org/vaeringjar/gnusocial-tracker/issues/16).  

Title: Projects Jam
Date: 2018-07-13 00:00
Modified: 2018-07-22 00:00
Category: blog
Tags: projects
Slug: projects-jam-20180713
Authors: zPlus
Description: Join us weekly on irc.freenode.net #peers on Thursday and/or Friday target start time around 19:00Z - 21:00Z to review of active Peers projects.
Summary: July 13th session of project jamming.

<!-- ********************************************************************** -->

### Highlights

This week we continued the discussion on the repository-federation project that
was initiated during the previous jam.  

The main topic being discussed was the license of GitPub, a work-in-progress
specification for federated VCS hosting maintained in a repository at GitHub by
people not participating in the Peers Community. The specification is released
under a non-free license and for this reason can't be merged with the
documentation in peers/repo-fed-voca.  

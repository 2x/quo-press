Title: Projects Jam
Date: 2018-04-06 00:00
Modified: 2018-04-17 16:16
Category: blog
Tags: projects
Slug: projects-jam-20180406
Authors: zPlus
Description: Join us weekly on irc.freenode.net #peers on Thursday and/or Friday target start time around 19:00Z - 21:00Z to review of active Peers projects.
Summary: April 6th session of project jamming.

### Highlights

I mostly worked on InternetRadio again, addressing more open issues.  

Fixed  
- https://notabug.org/metadb-apps/InternetRadio/issues/13
- https://notabug.org/metadb-apps/InternetRadio/issues/21 (I didn't plan to
fix this but I've probably found an easy solution)
- https://notabug.org/metadb-apps/InternetRadio/issues/27
- https://notabug.org/metadb-apps/InternetRadio/issues/29

Fixed at the previous jam, but now verified to work for newer Android APIs  
- https://notabug.org/metadb-apps/InternetRadio/issues/17
- https://notabug.org/metadb-apps/InternetRadio/issues/20
- https://notabug.org/metadb-apps/InternetRadio/issues/22

General maintenance of the Gradle build system to update old dependencies and
make F-Droid build the app succesfully.  

Update metadata and add screenshots for F-Droid.  

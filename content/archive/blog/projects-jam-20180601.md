Title: Projects Jam
Date: 2018-06-01 00:00
Modified: 2018-06-04 00:00
Category: blog
Tags: projects
Slug: projects-jam-20180601
Authors: vaeringjar
Description: Join us weekly on irc.freenode.net #peers on Thursday and/or Friday target start time around 19:00Z - 21:00Z to review of active Peers projects.
Summary: June 1st session of project jamming.

<!-- ---------------------------------------------------------------------- -->

### Highlights

* I worked on testing the build for [ffupdater](https://freepo.st/post/mct6c91dlm#comment-ss528ay4em).
* zPlus fixed a freepost issue [73 Use proper time tag](https://notabug.org/zPlus/freepost/issues/73).

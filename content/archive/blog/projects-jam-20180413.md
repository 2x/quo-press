Title: Projects Jam
Date: 2018-04-13 00:00
Modified: 2018-04-17 16:19
Category: blog
Tags: projects
Slug: projects-jam-20180413
Authors: zPlus
Description: Join us weekly on irc.freenode.net #peers on Thursday and/or Friday target start time around 19:00Z - 21:00Z to review of active Peers projects.
Summary: April 13th session of project jamming.

### Highlights

I've dedicated the time at the Jam to fix more InternetRadio's open issues.  

Fixed:  

- https://notabug.org/metadb-apps/InternetRadio/issues/14
- https://notabug.org/metadb-apps/InternetRadio/issues/24
- https://notabug.org/metadb-apps/InternetRadio/issues/31

Title: Projects Jam
Date: 2018-04-20 00:00
Modified: 2018-04-24 20:04
Category: blog
Tags: projects
Slug: projects-jam-20180420
Authors: vaeringjar
Description: Join us weekly on irc.freenode.net #peers on Thursday and/or Friday target start time around 19:00Z - 21:00Z to review of active Peers projects.
Summary: April 20th session of project jamming.

### Highlights

I continued to copy my hand-written notes on a design for an SSI interface that
leverages an OpenID Connect infrastructure.  

zPlus fixed https://notabug.org/zPlus/archive-webextension/issues/3 and also
uploaded to Mozilla addons website.  

Title: Projects Jam
Date: 2018-03-02 00:00
Modified: 2018-03-04 03:30
Category: blog
Tags: projects
Slug: projects-jam-20180302
Authors: vaeringjar
Description: Join us weekly on irc.freenode.net #peers on Thursday and/or Friday target start time around 19:00Z - 21:00Z to review of active Peers projects.
Summary: First full session of project jamming and a recap from the day before.

### Prologue

Yesterday, after taking a look at using spm, I decided I should take the next step in becoming more active from project to project. I [posted](https://freepo.st/post/8rqbdi1yup) my idea:  

>I want to start a casual weekly "let's take a look at a Peers project issue(s)" thing.
>As long as I do this alone, I think I'll do this either on Thursday or Friday, probably around my lunch which tends to happen around 19:00Z to 21:00Z. But if others want to plan a meeting time on #peers, let me know and we can schedule it.

Then I [answered a single question](https://notabug.org/kl3/spm/issues/4) on the spm issue tracker.  

### Kickoff

Today I decided to look at freedom-delayed, probably one of my favourite projects that isn't code. Overall, not much changed. I mainly used archive.org to verify old references and to save current version. I also used Wikipedia to see if anyone had referenced or discussed any of the projects. fr33domlover and bkeys joined in the chat, working on their own respective projects.  

### Minor Highlights

I had problems loading the reference to the *Dwarf Fortress* promise to free the code, so I [opened an issue](https://notabug.org/pizzaiolo/freedom-delayed/issues/15) on the tracker.  
Pocket might have or almost freed itself. Unsure as a non-user. https://github.com/Pocket/pocket-ff-addon
I also could not load zynaddsubfx.  

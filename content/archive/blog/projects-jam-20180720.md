Title: Projects Jam
Date: 2018-07-20 00:00
Modified: 2018-07-22 00:00
Category: blog
Tags: projects
Slug: projects-jam-20180720
Authors: zPlus
Description: Join us weekly on irc.freenode.net #peers on Thursday and/or Friday target start time around 19:00Z - 21:00Z to review of active Peers projects.
Summary: July 20th session of project jamming.

<!-- ********************************************************************** -->

### Highlights

This week we continued the discussion on the repository-federation project.
The issue was again the license of GitPub, which is expected to change to CC0
but at the moment is still a non-free license. For this reason it's still not
possible to merge the content of GitPub with peers/repo-fed-vocab.  

We talked about continuing the work of krt on ffupdater, "an Android app to
install and/or update Firefox for Android without using the Google Play Store".
Questions were risen regarding the possibility of non-free content being
included by Firefox, which cannot be filtered automatically by ffupdater.  

Worked to fix [https://notabug.org/metadb-apps/InternetRadio/issues/8#issuecomment-8981](https://notabug.org/metadb-apps/InternetRadio/issues/8#issuecomment-8981)  

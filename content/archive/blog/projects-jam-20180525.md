Title: Projects Jam
Date: 2018-05-25 00:00
Modified: 2018-05-30 00:00
Category: blog
Tags: projects
Slug: projects-jam-20180525
Authors: vaeringjar
Description: Join us weekly on irc.freenode.net #peers on Thursday and/or Friday target start time around 19:00Z - 21:00Z to review of active Peers projects.
Summary: May 25th session of project jamming.

<!-- ---------------------------------------------------------------------- -->

### Highlights

fr33domlover worked on vervis and the group gave feedback for the newest style
changes to the git diff.  

zPlus and I worked on a list of tasks for upcoming project jams.
zPlus listed some qualifiers.  

The best issues have these characteristics:  

- self contained, because there is only so much that can be done in 1H
- simple, that do not require a lot of previous knowledge to get started
- those that favor discussion and collaboration

Tasks:  

* https://notabug.org/peers/peers-www/issues/1
* https://notabug.org/pizzaiolo/freedom-delayed (check or add new entries)
* https://notabug.org/metadb-apps/InternetRadio/issues/33
* https://notabug.org/metadb-apps/InternetRadio/issues/30
* https://notabug.org/metadb-apps/InternetRadio/issues/25

* dokk: find high quality, maintained, RDF datasets. Alternatively, convert high
quality, maintained, non-RDF datasets to RDF. Worst scenario: make new dataset.
* awesome-gamedev: fork the project, make all merges and issue a patch.
https://github.com/Calinou/awesome-gamedev/network
* Plan for a time to play DMUX.
* peers-bounty-hunts: refine some of the project ideas and
https://notabug.org/peers/peers-bounty-hunts/issues/15
* dragora: build dragora on a BSD system and report back to selk.

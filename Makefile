# ---------- ---------- ---------- ---------- ---------- ----------
#
#   This makefile generates the peers website.
#
# ---------- ---------- ---------- ---------- ---------- ----------

TEMPLATES := lib/peers-template/

all: output

# ---------- ---------- ---------- ---------- ---------- ----------
#   NOTE: The remaining recipes follow:
# ---------- ---------- ---------- ---------- ---------- ----------

help:
	@echo "make           Build website"
	@echo "make server    Run a local test server"
	@echo "make clean     Delete build files and output"

lib:
	@test -d ${TEMPLATES} || git clone https://notabug.org/peers/peers-template.git ${TEMPLATES}

output: lib
	@pelican --settings pelicanconf.py --theme-path ${TEMPLATES}themes/pelican content/

server:
	@cd output && python3 -m http.server

clean:
	@rm --recursive --force lib output

.PHONY: all clean help output server

#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

from pelican import __version__ as PELICAN_VERSION

AUTHOR = u'vaeringjar'
#AVATAR = 'avatar.png'
SITENAME = u'Peers'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'America/Vancouver'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

SHOW_SIDEBAR = False

# Blogroll
LINKS = (
#            ('Pelican', 'http://getpelican.com/'),
#            ('Python.org', 'http://python.org/'),
#            ('Jinja2', 'http://jinja.pocoo.org/'),
#            ('You can modify those links in your config file', '#'),
         )

# Social widget
SOCIAL = (
#            ('You can add links in your config file', '#'),
#            ('Another social link', '#'),
          )


DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
ARTICLE_URL = 'archive/{date:%Y}{date:%m}{date:%d}/{slug}/'
ARTICLE_SAVE_AS = ARTICLE_URL + 'index.html'
AUTHOR_URL = 'author/{slug}/'
AUTHOR_SAVE_AS = AUTHOR_URL + 'index.html'
#CATEGORY_URL = 'category/{category}/'
#CATEGORY_SAVE_AS = 'category/{category}/index.html'
PAGE_URL = '{slug}/'
PAGE_SAVE_AS = PAGE_URL + 'index.html'

# Static paths for media
PATH = 'content'
STATIC_PATHS = [
    'extra',
    'media'
    ]
EXTRA_PATH_METADATA = {
    'media/hack.png': {'path': 'favicon.ico'},
    'media/PeersCo_websitebanner_v3.png': {'path': 'banner.png'},
    'media/glider-circles.svg': {'path': 'logo.svg'},
    'extra/.htaccess': {'path': '.htaccess'},
}
